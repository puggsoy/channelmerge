# ChannelMerge v0.1

ChannelMerge is a program that merges two audio WAV files into one stereo WAV file, using one file for each channel. It can do this with multiple files at a time, making it quicker and easier to use when needing to do this with a large number of files.

## Current features

* Base functionality, current build was quickly put together

## Future features

* Improve stability and error checking
* Remove vgmstream, should support only WAV files
* Dialogue boxes to select folders using waxe (?)