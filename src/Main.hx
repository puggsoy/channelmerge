package ;

import format.wav.Writer;
import haxe.io.Path;
import haxe.Log;
import neko.Lib;
import sys.FileSystem;
import format.wav.Data;
import sys.io.FileOutput;
import sys.io.File;

/**
 * ...
 * @author Puggsoy
 */

class Main 
{
	static private var _args:Array<String>;
	static private var _inDir:Path;
	static private var _outDir:Path;
	static private var _lefts:Array<Path>;
	static private var _rights:Array<Path>;
	
	static private function main() 
	{
		_args = Sys.args();
		
		if (_args.length < 1)
		{
			end("Usage: ChannelMerge inDir [outDir]\n    inDir: Directory containing the files to merge.\n    outDir: Directory in which to save the results.");
		}
		else
		{
			if (!FileSystem.exists(_args[0]) || !FileSystem.isDirectory(_args[0]))
			{
				end("First parameter must be a directory!");
				return;
			}
			
			_inDir = new Path(_args[0]);
			
			if (_args[1] != null)
			{
				if (!FileSystem.exists(_args[0]) || !FileSystem.isDirectory(_args[0]))
				{
					end("Second parameter must be a directory!");
					return;
				}
				
				_outDir = new Path(_args[1]);
			}
			else
			{
				_outDir = new Path(_args[0] + "/merged");
			}
			
			loadFiles();
		}
	}
	
	static private function loadFiles()
	{
		var contents:Array<String> = FileSystem.readDirectory(_inDir.toString());
		
		var vgmStream:VGMStream = new VGMStream("vgmstream/vgmstream.exe");
		
		_lefts = new Array<Path>();
		_rights = new Array<Path>();
		
		for (file in contents)
		{
			var path:Path = new Path(_inDir.toString() + "/" + file);
			if (!FileSystem.isDirectory(path.toString()))
			{
				var prefix:String = path.file.substr(0, path.file.length - 1);
				var suffix:String = file.substr(path.file.length - 1, 1);
				
				if (suffix.toUpperCase() == "L")
				{
					_lefts.push(path);
				}
				else
				if (suffix.toUpperCase() == "R")
				{
					_rights.push(path);
				}
				else
				{
					Log.trace(file + " doesn't end with R or L");
				}
			}
		}
		
		filterToMatch(_lefts, _rights);
		filterToMatch(_rights, _lefts);
		_lefts.sort(srtPaths);
		_rights.sort(srtPaths);
		
		merge();
	}
	
	static private function merge()
	{
		var vgmstream:VGMStream = new VGMStream("vgmstream/vgmstream.exe");
		
		for (i in 0..._lefts.length)
		{
			Log.trace("merging " + _lefts[i] + " and " + _rights[i]);
			
			var left:WAVE = vgmstream.convert(_lefts[i].toString(), true);
			var right:WAVE = vgmstream.convert(_rights[i].toString(), true);
			
			var merged:WAVE = Merger.merge(left, right);
			
			if (!FileSystem.exists(_outDir.toString()))
			{
				FileSystem.createDirectory(_outDir.toString());
			}
			
			var outPath:Path = new Path(_outDir + "/" + _lefts[i].file.substr(0, _lefts[i].file.length - 1) + ".wav");
			
			Log.trace("saving " + outPath.toString());
			
			saveFile(merged, outPath);
		}
	}
	
	static private function saveFile(file:WAVE, outPath:Path)
	{
		var out:FileOutput = File.write(outPath.toString());
		var w:Writer = new Writer(out);
		w.write(file);
	}
	
	static private function srtPaths(a:Path, b:Path):Int
	{
		var sa:String = a.file + "." + a.ext;
		var sb:String = b.file + "." + b.ext;
		
		return Reflect.compare(sa.toLowerCase(), sb.toLowerCase());
	}
	
	static private function filterToMatch(a:Array<Path>, b:Array<Path>)
	{
		for (p1 in a)
		{
			var found:Bool = false;
			
			for (p2 in b)
			{
				if (p1.file.substr(0, p1.file.length - 1) == p2.file.substr(0, p2.file.length - 1))
				{
					found = true;
				}
			}
			
			if (!found)
			{
				Log.trace(p1.file + "." + p1.ext + " doesn't have a match");
				a.remove(p1);
			}
		}
	}
	
	static private function end(?msg:String)
	{
		if (msg != null)
		{
			Sys.println(msg);
		}
		
		Sys.getChar(false);
	}
}