package ;
import format.wav.Data.WAVE;
import format.wav.Data.WAVEHeader;
import haxe.io.Bytes;
import haxe.io.BytesInput;
import haxe.io.BytesOutput;
import haxe.io.Path;
import haxe.Log;

/**
 * ...
 * @author Puggsoy
 */
class Merger
{
	
	static public function merge(left:WAVE, right:WAVE):WAVE
	{
		if (left.data.length != right.data.length)
		{
			Log.trace("Cannot merge files: different data lengths");
			return null;
		}
		
		var leftBytes:BytesInput = new BytesInput(left.data);
		var rightBytes:BytesInput = new BytesInput(right.data);
		var mergedBytes:BytesOutput = new BytesOutput();
		
		for (i in 0...Math.floor(left.data.length / 4))
		{
			var leftChan:Int = Math.round((leftBytes.readInt16() + leftBytes.readInt16()) / 2);
			var rightChan:Int = Math.round((rightBytes.readInt16() + rightBytes.readInt16()) / 2);
			
			mergedBytes.writeInt16(leftChan);
			mergedBytes.writeInt16(rightChan);
		}
		
		var mergedHeader:WAVEHeader = left.header;
		var mergedData:Bytes = mergedBytes.getBytes();
		
		var mergedWav:WAVE = { header: mergedHeader, data: mergedData };
		
		return mergedWav;
	}
}