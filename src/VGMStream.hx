package ;
import format.wav.Data.WAVE;
import format.wav.Reader;
import haxe.io.Bytes;
import haxe.io.BytesInput;
import haxe.io.BytesOutput;
import haxe.Log;
import sys.io.Process;

/**
 * ...
 * @author Puggsoy
 */
class VGMStream
{
	private var _exePath:String;
	
	public function new(exePath:String) 
	{
		_exePath = exePath;
	}
	
	public function convert(path:String, pipe:Bool, ?outDir:String):WAVE
	{
		if (pipe)
		{
			var process:Process = new Process(_exePath, ["-p", path]);
			
			var inWav:WAVE = new Reader(process.stdout).read();
			
			process.close();
			return inWav;
		}
		else
		if (outDir != null)
		{
			var process:Process = new Process(_exePath, ["-o " + outDir, path]);
			
			process.close();
		}
		
		return null;
	}
}